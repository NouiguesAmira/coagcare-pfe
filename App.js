
import * as React from 'react';
import { View, Text,Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Inscription from './Components/Inscription';
import Login from './Components/Login';
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'CoagCare',
            headerStyle: {
              backgroundColor: 'skyblue',
            },
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerTintColor: '#fff',
          }}
        />

        <Stack.Screen
                  name="Details"
                  component={DetailsScreen}
                  options={{
                    title: 'My home',
                    headerStyle: {
                      backgroundColor: '#f4511e',
                    },
                    headerTitleStyle: {
                                fontWeight: 'bold',
                              },
                    headerTintColor: '#fff',
                  }}
                />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

function HomeScreen({ navigation }) {
  return (
  <Login />

  );
}



function DetailsScreen({ navigation }) {
  return (
    <Inscription/>
  );
}


export default App;
