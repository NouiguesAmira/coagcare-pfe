export default {
  oidc: {
    clientId: '0oa34juu1WWumGJEb4x6',
    redirectUri: 'com.okta.dev-420738:/callback',
    endSessionRedirectUri: 'com.okta.dev-420738:/callback',
    discoveryUri: 'https://dev-420738.okta.com/oauth2/default',
    scopes: ['openid', 'profile', 'offline_access'],
    requireHardwareBackedKeyStore: false,
  },
};
