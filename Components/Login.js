import * as React from 'react';
import { View, TextInput, StyleSheet ,Image ,Button, TouchableOpacity, Text} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';

class Login extends React.Component {

  render() {
  return (
   <View style={{ flex: 1, backgroundColor: '#E9E9E9' }}>

            <View style = {{ paddingTop:70 , alignItems: 'center',justifyContent: 'center'}}>
                <Image source={require('../Images/logo.jpg')} style={styles.ImageLogo} />
            </View>

      <View style = {{ paddingTop:30 }}>

     <View style={styles.SectionStyle} >
       <Image source={require('../Images/mail.png')} style={styles.ImageStyle} />
               <TextInput
                   style={{flex:1}}
                   placeholder="E-mail"
                   underlineColorAndroid="transparent"
               />
     </View>
     <View style={styles.SectionStyle}>
            <Image source={require('../Images/p1.png')} style={styles.ImageStyle} />
                    <TextInput
                        style={{flex:1}}
                        placeholder="Mot De Passe"
                        underlineColorAndroid="transparent"
                    />
     </View>
             <View style={{ flex: 1, flexDirection: 'row' }}>
                 <View style={{ marginTop: 30 ,width : 250, height : 50 , left : 10 ,margin :20, flexDirection: 'row'  }}>
                     <Button color="#9FCFF9"  title="Se Connecter" onPress={() => {}} large></Button>
                 </View>
                 <View style={{ marginTop: 30 ,width : 250, height : 50  ,right:100,margin :20, flexDirection: 'row'  }}>
                     <Button color="#9FCFF9"  title="     S'inscrire   " onPress={() => navigation.navigate('Inscription')} large></Button>
                 </View>
             </View>
     </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
 SectionStyle: {

    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#A7A8A9',
    height: 40,
    margin:15,
    left: 6
  },
  ImageStyle: {
      padding: 10,
      margin: 5,
      height: 25,
      width: 25,
      resizeMode : 'stretch',
      alignItems: 'center'
  },

  ImageLogo: {
        padding: 10,
        margin: 5,
        height: 100,
        width: 150,
        borderColor:'#E9E9E9'
        ,borderWidth:5,
        backfaceVisibility :'hidden',
        backgroundColor:'#E9E9E9',
        borderRadius: 20,
    },

  button: {
    flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: .5,
      borderColor: '#F0F4F9',
      height: 200,
      borderRadius: 5 ,
      margin: 5
  }
})

export default Login